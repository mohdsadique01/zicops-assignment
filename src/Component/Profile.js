import React, { useEffect } from 'react';
import { useState } from 'react';
function Profile({settingCount, exitFromLab, remainingTime}) {
  let value = remainingTime >0? remainingTime: 10;
  let [counter, setCounter] = useState(value);
 const [running, setRunning]= useState(true);                                                      

  useEffect(()=>{
    let interval =0;
    if(running) {

       interval= setInterval(() => {
        counter = counter - 1;
        setCounter(counter);
        if(counter<0){
          clearInterval(interval);
          exitFromLab(false);
        }
      },60000);
    }
    return ()=> clearInterval(interval);
  },[running])
 
  
  const HandleClick = ()=>{
    exitFromLab(false);
    settingCount(counter);
    // clearInterval(counter);
    setRunning(false);
  }
  return (
    <div >
   
      <h1>Counter: {counter}</h1>
      <button className='profile' onClick={HandleClick}>Exit button</button>
    </div>
  );
}

export default Profile

