import React,{useEffect ,useState} from 'react'
import {  useNavigate } from 'react-router-dom'
import Main from './Main';
// import {use}
import './Login.css'
import Profile from './Profile'

function Login() {
  let [newpassword,setNewpassword]= useState(123456789);
  let [isProfle,setProfile ]= useState(false);
  let [counterTime, setCounterTime] = useState(0);
  const [password,setPassword]=useState('');
  const [running, setRunning]= useState(false); 
  const HandleChange = e=>{
    setPassword(e.target.value)
  }
  const UpdateCount = (count) => {
    if(count>0) { 
      setRunning(true);
    } else {
      setRunning(false);
    }
    setPassword('')
    setCounterTime(count);
    
  }
 
  useEffect(()=>{
    let interval =0;
    if(running) {
       interval= setInterval(() => {
        counterTime = counterTime - 1;
        setCounterTime(counterTime);
        if(counterTime<0){
          setNewpassword(987654321);
          clearInterval(interval);
        }
      },60000);
    }
    return()=> clearInterval(interval);
  } ,[running])
  
  const ExitLab = (value) => {
    setProfile(value);
  }
  const HandleClick= ()=>{
    console.log(password,newpassword,'22222')
   if(password==newpassword){

    setProfile(true)
    
   }else{
    setProfile(false)
    alert("You have entered an incorrect password") 
   }
  }
 
 if(isProfle) {
  return <Profile settingCount={UpdateCount} exitFromLab={ExitLab} remainingTime={counterTime}/>
 } else {

   return (
     <div>
      <div className='home'>
      <Main data= {counterTime} />

    </div>
     
     <div className='login-main' >
      
       <div className='login-box'>
     
         <div className='img-box'>
             <img className='img' src='https://img.icons8.com/cotton/2x/fingerprint.png' alt=''/>
         </div>
        
         <span className="spn">ENTER YOUR PILEARNING PASSWORD</span>
         <input className='inpt'  type = {"password"} onChange={HandleChange} /><br></br>
         
         <button   className='btn'  onClick={HandleClick} >Enter Lab</button>
         
       </div>
     </div>
     </div>
   )
 }

}

export default Login
